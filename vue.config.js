// Arrange for links to local resources to all start with "/register" so we can
// serve it statically from express.js mounted at "register".

// See https://medium.com/developer-rants/why-is-strict-mime-type-checking-blocking-the-static-serving-of-vue-frontend-files-4cbea1eedbd1

module.exports = {
  publicPath: (process.env.NODE_ENV === 'production' || process.env.VUE_APP_BOT_SERVED === 'true')
    ? '/register/'
    : '/'
}
